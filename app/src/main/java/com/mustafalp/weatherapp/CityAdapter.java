package com.mustafalp.weatherapp;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mustafalp.weatherapp.models.City;

import java.util.ArrayList;
import java.util.List;

public class CityAdapter extends  RecyclerView.Adapter<CityAdapter.MyViewHolder> {

    private List<String> cityList;
    int selectedPosition;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView city;

        public MyViewHolder(View view) {
            super(view);
            city = (TextView) view.findViewById(R.id.city);

        }
    }


    public CityAdapter(List<String> cityList) {
        this.cityList = cityList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        String cityName = cityList.get(position);
        holder.city.setText(cityName);

        if (holder.getAdapterPosition() == selectedPosition) {
            holder.itemView.setBackgroundColor(Color.BLUE);
        } else {
            holder.itemView.setBackgroundColor(Color.WHITE);
        }


        holder.city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition = holder.getAdapterPosition();
                notifyDataSetChanged();


            }
        });

    }

    @Override
    public int getItemCount() {
        return cityList.size();
    }

}