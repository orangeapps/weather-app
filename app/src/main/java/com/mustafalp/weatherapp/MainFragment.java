package com.mustafalp.weatherapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mustafalp.weatherapp.models.WeatherAPI;
import com.mustafalp.weatherapp.models.WeatherDay;
import com.mustafalp.weatherapp.models.WeatherForecast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainFragment extends HomeFragment {

    TextView city, status, humidity, pressure;
    String url = "http://api.openweathermap.org/data/2.5";

    String TAG = "WEATHER";
    TextView tvTemp;
    TextView cityText;
    TextView lowestText;
    TextView highestText;
    ImageView tvImage;
    ImageView lowestImage;
    ImageView highestImage;
    LinearLayout llForecast;
    WeatherAPI.ApiInterface api;

    public static MainFragment newInstance() {

        MainFragment fragment = new MainFragment();
        return fragment;


    }


    @Override
    protected void onFragmentLoad() {
        super.onFragmentLoad();
        getActivity().setTitle("Weather");


        tvTemp = (TextView) getView().findViewById(R.id.tvTemp);
        tvImage = (ImageView) getView().findViewById(R.id.ivImage);
        llForecast = (LinearLayout) getView().findViewById(R.id.llForecast);
        cityText =(TextView) getView().findViewById(R.id.textCity);
        lowestText = (TextView) getView().findViewById(R.id.textLowest);
        highestText = (TextView) getView().findViewById(R.id.textHighest);
        lowestImage = (ImageView) getView().findViewById(R.id.lowestImage);
        highestImage = (ImageView) getView().findViewById(R.id.highestImage);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        String cityName = preferences.getString("SelectedCity", "Izmir");

        api = WeatherAPI.getClient().create(WeatherAPI.ApiInterface.class);
        getWeather(cityName);

    }

    public void getWeather(String cityName) {

        String city = cityName+",tr";

        String units = "metric";
        String key = WeatherAPI.KEY;

        Log.d(TAG, "OK");

        // get weather for today
        Call<WeatherDay> callToday = api.getToday(city, units, key);
        callToday.enqueue(new Callback<WeatherDay>() {
            @Override
            public void onResponse(Call<WeatherDay> call, Response<WeatherDay> response) {
                Log.e(TAG, "onResponse");
                WeatherDay data = response.body();
                //Log.d(TAG,response.toString());


                if (response.isSuccessful()) {
                    tvTemp.setText( data.getTempMin().substring(0,data.getTempMin().indexOf("."))+ "°" + " / " + data.getTempMax().substring(0,data.getTempMax().indexOf("."))+ "°" );
                    cityText.setText(data.getCity() + " / " + data.getDescription());
                    lowestText.setText(data.getTempMin().substring(0,data.getTempMin().indexOf("."))+ "°" );
                    highestText.setText(data.getTempMax().substring(0,data.getTempMax().indexOf("."))+ "°" );
                    Glide.with(getContext()).load(data.getIconUrl()).into(tvImage);
                    Glide.with(getContext()).load(data.getIconUrl()).into(lowestImage);
                    Glide.with(getContext()).load(data.getIconUrl()).into(highestImage);
                }
            }

            @Override
            public void onFailure(Call<WeatherDay> call, Throwable t) {
                Log.e(TAG, "onFailure");
                Log.e(TAG, t.toString());
            }
        });

        // get weather forecast
        Call<WeatherForecast> callForecast = api.getForecast(city, units, key);
        callForecast.enqueue(new Callback<WeatherForecast>() {
            @Override
            public void onResponse(Call<WeatherForecast> call, Response<WeatherForecast> response) {
                Log.e(TAG, "onResponse");
                WeatherForecast data = response.body();
                //Log.d(TAG,response.toString());

                if (response.isSuccessful()) {
                    SimpleDateFormat formatDayOfWeek = new SimpleDateFormat("E");
                    LinearLayout.LayoutParams paramsTextView = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    LinearLayout.LayoutParams paramsImageView = new LinearLayout.LayoutParams(convertDPtoPX(40, getContext()),
                            convertDPtoPX(40, getContext()));

                    int marginRight = convertDPtoPX(15, getContext());
                    LinearLayout.LayoutParams paramsLinearLayout = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    paramsLinearLayout.setMargins(0, 0, marginRight, 0);

                    llForecast.removeAllViews();

                    for (WeatherDay day : data.getItems()) {
                        if (day.getDate().get(Calendar.HOUR_OF_DAY) == 15) {
                            String date = String.format("%d.%d.%d %d:%d",
                                    day.getDate().get(Calendar.DAY_OF_MONTH),
                                    day.getDate().get(Calendar.WEEK_OF_MONTH),
                                    day.getDate().get(Calendar.YEAR),
                                    day.getDate().get(Calendar.HOUR_OF_DAY),
                                    day.getDate().get(Calendar.MINUTE)
                            );
                            Log.d(TAG, date);
                            Log.d(TAG, day.getTempInteger());
                            Log.d(TAG, "---");

                            // child view wrapper
                            LinearLayout childLayout = new LinearLayout(getContext());
                            childLayout.setLayoutParams(paramsLinearLayout);
                            childLayout.setOrientation(LinearLayout.VERTICAL);

                            // show day of week
                            TextView tvDay = new TextView(getContext());
                            String dayOfWeek = formatDayOfWeek.format(day.getDate().getTime());
                            tvDay.setText(dayOfWeek);
                            tvDay.setLayoutParams(paramsTextView);
                            childLayout.addView(tvDay);

                            // show image
                            ImageView ivIcon = new ImageView(getContext());
                            ivIcon.setLayoutParams(paramsImageView);
                            Glide.with(getContext()).load(day.getIconUrl()).into(ivIcon);
                            childLayout.addView(ivIcon);

                            // show temp
                            TextView tvTemp = new TextView(getContext());
                            tvTemp.setText(day.getTempWithDegree());
                            tvTemp.setLayoutParams(paramsTextView);
                            childLayout.addView(tvTemp);

                            llForecast.addView(childLayout);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<WeatherForecast> call, Throwable t) {
                Log.e(TAG, "onFailure");
                Log.e(TAG, t.toString());
            }
        });

    }

    public int convertDPtoPX(int dp, Context ctx) {
        float density = ctx.getResources().getDisplayMetrics().density;
        int px = (int)(dp * density);
        return px;
    }




    @Override
    public int getLayoutResId() {
        return R.layout.fragment_main;
    }
}
