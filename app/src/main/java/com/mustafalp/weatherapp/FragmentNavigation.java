package com.mustafalp.weatherapp;

import android.support.v4.app.Fragment;

public interface FragmentNavigation {
    void pushFragment(Fragment fragment);

    void replaceFragment(Fragment fragment);

    void clearStack();

    void popFragment(int depth);
}
