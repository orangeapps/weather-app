package com.mustafalp.weatherapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.Arrays;

public class SettingsFragment extends HomeFragment {

    public static SettingsFragment newInstance() {

        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    private RecyclerView recyclerView;
    private CityAdapter mAdapter;
    private Button button;
    private ProgressBar mProgressBar;
    private Switch simpleSwitch;
    private int mProgressStatus = 0;

    @Override
    public void onResume() {
        super.onResume();

        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();


        simpleSwitch.setChecked(isConnected);

        simpleSwitch.setClickable(false);
    }

    @Override
    protected void onFragmentLoad() {
        super.onFragmentLoad();
        getActivity().setTitle("Settings");
        ArrayList<String> cityArray = new ArrayList<String>(
                Arrays.asList("Adana", "Adıyaman", "Afyon", "Ağrı", "Amasya", "Ankara", "Antalya", "Artvin",
                        "Aydın", "Balıkesir", "Bilecik", "Bingöl", "Bitlis", "Bolu", "Burdur", "Bursa", "Çanakkale",
                        "Çankırı", "Çorum", "Denizli", "Diyarbakır", "Edirne", "Elazığ", "Erzincan", "Erzurum", "Eskişehir",
                        "Gaziantep", "Giresun", "Gümüşhane", "Hakkari", "Hatay", "Isparta", "Mersin", "İstanbul", "Izmir",
                        "Kars", "Kastamonu", "Kayseri", "Kırklareli", "Kırşehir", "Kocaeli", "Konya", "Kütahya", "Malatya",
                        "Manisa", "Kahramanmaraş", "Mardin", "Muğla", "Muş", "Nevşehir", "Niğde", "Ordu", "Rize", "Sakarya",
                        "Samsun", "Siirt", "Sinop", "Sivas", "Tekirdağ", "Tokat", "Trabzon", "Tunceli", "Şanlıurfa", "Uşak",
                        "Van", "Yozgat", "Zonguldak", "Aksaray", "Bayburt", "Karaman", "Kırıkkale", "Batman", "Şırnak",
                        "Bartın", "Ardahan", "Iğdır", "Yalova", "Karabük", "Kilis", "Osmaniye", "Düzce"));




        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        String cityName = preferences.getString("SelectedCity", "Izmir");

        recyclerView = (RecyclerView) getView().findViewById(R.id.recycler_view);
        mProgressBar = (ProgressBar) getView().findViewById(R.id.pb);
        simpleSwitch = (Switch) getView().findViewById(R.id.wifiSwitch);

        IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);

        getActivity().getApplicationContext().registerReceiver(mBroadcastReceiver, iFilter);

        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();


        simpleSwitch.setChecked(isConnected);

        simpleSwitch.setClickable(false);
        simpleSwitch.refreshDrawableState();
        mAdapter = new CityAdapter(cityArray);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        mAdapter.selectedPosition = cityArray.indexOf(cityName);
        recyclerView.scrollToPosition(mAdapter.selectedPosition);
        button = (Button) getView().findViewById(R.id.save);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                SharedPreferences.Editor editor = preferences.edit();

                editor.putString("SelectedCity", cityArray.get(mAdapter.selectedPosition));
                editor.commit();

                Intent intent = new Intent(view.getContext(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                view.getContext().startActivity(intent);


            }
        });


    }

    @Override
    public int getLayoutResId() {
        return R.layout.fragment_settings;
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            /*
                BatteryManager
                    The BatteryManager class contains strings and constants used for values in the
                    ACTION_BATTERY_CHANGED Intent, and provides a method for querying battery
                    and charging properties.
            */

            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);

            // Calculate the battery charged percentage
            float percentage = level / (float) scale;
            // Update the progress bar to display current battery charged percentage
            mProgressStatus = (int) ((percentage) * 100);
            mProgressBar.setProgress(mProgressStatus);
        }
    };


}
