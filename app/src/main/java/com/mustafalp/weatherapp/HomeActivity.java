package com.mustafalp.weatherapp;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import butterknife.BindView;

public class HomeActivity extends HomeBaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        navigationView = findViewById(R.id.navigation);
        init();
    }


    BottomNavigationView navigationView;

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        this.selectedIndex = item.getOrder();

        switch (item.getItemId()) {
            case R.id.navigation_main:
                return navigatePage(0);
            case R.id.navigation_camera:
                return navigatePage(1);
            case R.id.navigation_settings:
                return navigatePage(2);
        }
        return false;
    }

    private boolean navigatePage(int pageIndex) {
        fragmentNavigationController.switchTab(pageIndex);

        return true;
    }

    private void init() {


        navigationView.setSelectedItemId(R.id.navigation_main);

        navigationView.setOnNavigationItemSelectedListener(this);

    }


    public BottomNavigationView getBottomBar() {
        return navigationView;
    }

}
