package com.mustafalp.weatherapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.ncapdevi.fragnav.FragNavController;
import com.ncapdevi.fragnav.FragNavTransactionOptions;
import com.ncapdevi.fragnav.tabhistory.FragNavTabHistoryController;

import butterknife.ButterKnife;

public abstract class HomeBaseActivity extends AppCompatActivity implements FragmentNavigation, FragNavController.TransactionListener, FragNavController.RootFragmentListener {

    public static final int DEFAULT_SELECTED_PAGE_INDEX = 1;

    public static final int INDEX_MAIN = FragNavController.TAB1;
    public static final int INDEX_CAMERA = FragNavController.TAB2;
    public static final int INDEX_SETTINGS= FragNavController.TAB3;
    private static final int NUMBER_OF_TABS = 3;


    protected FragNavController fragmentNavigationController;

    protected int selectedIndex = FragNavController.TAB1;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);


        // initialize fragment navigation Controller
        fragmentNavigationController = FragNavController.newBuilder(savedInstanceState, getSupportFragmentManager(), R.id.home_content)
                .defaultTransactionOptions(FragNavTransactionOptions.newBuilder().allowStateLoss(true).build())
                .transactionListener(this)
                .selectedTabIndex(selectedIndex)
                .rootFragmentListener(this, NUMBER_OF_TABS)
                .popStrategy(FragNavTabHistoryController.UNIQUE_TAB_HISTORY)
                .switchController((index, transactionOptions) -> {
                    Log.d("fragmentNavigation", "onCreate: " + index);
                })
                .build();
    }




    @Override
    public void pushFragment(Fragment fragment) {
        if (isFinishing()) {
            return;
        }
        fragmentNavigationController.pushFragment(fragment);
    }

    @Override
    public void replaceFragment(Fragment fragment) {
        if (isFinishing()) {
            return;
        }
        fragmentNavigationController.replaceFragment(fragment);
    }

    @Override
    public void popFragment(int depth) {
        fragmentNavigationController.popFragments(depth);
    }

    @Override
    public void clearStack() {
        fragmentNavigationController.clearStack();
    }




    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (fragmentNavigationController != null) {
            fragmentNavigationController.onSaveInstanceState(outState);
        }
        super.onSaveInstanceState(outState);
    }


    @Override
    public Fragment getRootFragment(int index) {
        switch (index) {
            case INDEX_MAIN:
                return MainFragment.newInstance();
            case INDEX_CAMERA:
                return CameraFragment.newInstance();
            case INDEX_SETTINGS:
                return SettingsFragment.newInstance();
        }

        throw new IllegalStateException("Need to send an index that we know");
    }

    @Override
    public void onTabTransaction(@Nullable android.support.v4.app.Fragment fragment, int tabIndex) {

    }

    @Override
    public void onFragmentTransaction(android.support.v4.app.Fragment fragment, FragNavController.TransactionType transactionType) {

    }


}
