package com.mustafalp.weatherapp;

import android.content.Context;

public abstract class HomeFragment extends com.mustafalp.weatherapp.Fragment {

    public FragmentNavigation fragmentNavigation;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentNavigation) {
            this.fragmentNavigation = (FragmentNavigation) context;
        }

    }

}
